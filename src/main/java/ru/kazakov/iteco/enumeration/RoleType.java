package ru.kazakov.iteco.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
public enum RoleType {

    DEFAULT("default"),
    ADMINISTRATOR("Administrator");

    @Getter
    @NotNull
    private String displayName;

}
