package ru.kazakov.iteco.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;

public final class Password {

    @NotNull
    public static String getHashedPassword(@Nullable final String password) throws Exception {
        if (password == null) throw new Exception();
        final String salt = "@JF27$o%";
        String hashed = password;
        for (int i = 0; i < 60000 ; i++) {
            hashed = salt + hashed + salt;
            hashed = md5Custom(hashed);
        }
        return hashed;
    }

    @NotNull
    private static String md5Custom(final @Nullable String password) throws Exception {
        if (password == null) throw new Exception();
        final MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        final byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toLowerCase();
    }

}
