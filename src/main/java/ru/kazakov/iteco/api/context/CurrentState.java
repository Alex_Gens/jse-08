package ru.kazakov.iteco.api.context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.entity.User;
import java.util.List;

public interface CurrentState {

    @Nullable
    public User getCurrentUser();

    public void setCurrentUser(@Nullable User currentUser);

    @NotNull
    public List<AbstractCommand> getCommands();

}
