package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Project;
import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    public String getName(@Nullable final String id) throws Exception;

    public void remove(@Nullable final List<String> ids) throws Exception;

    public void removeAll(@Nullable final String currentUserId) throws Exception;

    @Nullable
    public Project findByName(@Nullable final String name) throws Exception;

    @Nullable
    public Project findByName(@Nullable final String name, @Nullable final String currentUserId) throws Exception;

    @Nullable
    public List<Project> findAll(@Nullable final List<String> ids) throws Exception;

    @Nullable
    public List<Project> findAll(@Nullable final String currentUserId) throws Exception;

    public boolean contains(@Nullable final String name) throws Exception;

    public boolean contains(@Nullable final String name, @Nullable final String currentUserId) throws Exception;

    public boolean isEmpty(@Nullable final String id) throws Exception;

}
