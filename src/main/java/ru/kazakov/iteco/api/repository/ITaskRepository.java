package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @Nullable
    public String getName(@NotNull final String id);

    @Nullable
    public String getProjectId(@NotNull final String id);

    public void setProjectId(@NotNull final String id, @NotNull final String projectId);

    public void remove(@NotNull final List<String> ids);

    public void removeWithProject(@NotNull final String currentUserId, @NotNull final String projectId);

    public void removeAll(@NotNull final String currentUserId);

    public void removeAllWithProjects(@NotNull final String currentUserId);

    @Nullable
    public Task findByName(@NotNull final String name);

    @Nullable
    public Task findByName(@NotNull final String name, @NotNull final String currentUserId);

    @NotNull
    public List<Task> findAll(@NotNull final List<String> ids);

    @NotNull
    public List<Task> findAll(@NotNull final String currentUserId);

    public boolean contains(@NotNull final String name);

    public boolean contains(@NotNull final String name, @NotNull final String currentUserId);

    public boolean isEmpty(@NotNull final String id);

}
