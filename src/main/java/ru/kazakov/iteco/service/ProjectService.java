package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.entity.Project;
import java.util.List;

public class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    private final IProjectRepository repository;

    public ProjectService(@NotNull IProjectRepository projectRepository) {this.repository = projectRepository;}

    @Nullable
    @Override
    public String getName(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.getName(id);
    }

    @Override
    public void remove(@Nullable final List<String> ids) throws Exception {
        if (ids == null) throw new Exception();
        repository.remove(ids);
    }

    @Override
    public void removeAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        repository.removeAll(currentUserId);
    }

    @Nullable
    @Override
    public Project findByName(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        return repository.findByName(name);
    }

    @Nullable
    @Override
    public Project findByName(@Nullable final String name, @Nullable final String currentUserId) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findByName(name, currentUserId);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final List<String> ids) throws Exception {
        if (ids == null) throw new Exception();
        return repository.findAll(ids);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findAll(currentUserId);
    }

    @Override
    public boolean contains(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        return repository.contains(name);
    }

    @Override
    public boolean contains(@Nullable final String name, @Nullable final String currentUserId) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.contains(name, currentUserId);
    }

    @Override
    public boolean isEmpty(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.isEmpty(id);
    }

    @NotNull
    @Override
    protected IProjectRepository getRepository() {return repository;}

}
