package ru.kazakov.iteco.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
public abstract class AbstractEntity {

    @NotNull protected String id = UUID.randomUUID().toString();

}
