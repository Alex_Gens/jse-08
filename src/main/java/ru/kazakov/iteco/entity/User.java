package ru.kazakov.iteco.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.RoleType;
import java.util.Date;

@Data
@NoArgsConstructor
public final class User extends AbstractEntity  {

    @Nullable
    private String name = "";

    @Nullable
    private String login = "";

    @Nullable
    private String password;

    @Nullable
    private Date dateStart = new Date();

    @Nullable
    private Date dateFinish = new Date();

    @NotNull
    private RoleType roleType = RoleType.DEFAULT;

}
