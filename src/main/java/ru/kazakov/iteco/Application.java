package ru.kazakov.iteco;

import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.command.project.*;
import ru.kazakov.iteco.command.system.AboutCommand;
import ru.kazakov.iteco.command.system.ExitCommand;
import ru.kazakov.iteco.command.system.HelpCommand;
import ru.kazakov.iteco.command.task.*;
import ru.kazakov.iteco.command.user.*;
import ru.kazakov.iteco.context.Bootstrap;

public final class Application {

    public static void main(@NotNull String[] args) throws Exception {
        @NotNull final Class<?>[] classes = {ProjectCreateCommand.class, ProjectGetCommand.class, ProjectUpdateCommand.class,
                              ProjectRemoveCommand.class, ProjectListCommand.class, ProjectAddTaskCommand.class,
                              ProjectAddTaskCommand.class, ProjectListTasksCommand.class, ProjectClearCommand.class,
                              TaskCreateCommand.class, TaskGetCommand.class, TaskUpdateCommand.class,
                              TaskRemoveCommand.class, TaskListCommand.class, TaskClearCommand.class,
                              UserLoginCommand.class, UserChangePasswordCommand.class, UserLogoutCommand.class,
                              UserLogoutCommand.class, UserGetCommand.class, UserCreateCommand.class,
                              UserCreateCommand.class, UserUpdateCommand.class, UserListCommand.class,
                              UserListCommand.class, HelpCommand.class, ExitCommand.class,
                              ExitCommand.class, AboutCommand.class};
        new Bootstrap().init(classes);
    }

}
