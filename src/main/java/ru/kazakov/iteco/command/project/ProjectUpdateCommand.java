package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Project;

@NoArgsConstructor
public final class ProjectUpdateCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-update";

    @Getter
    @NotNull
    private final String description = "Update project information.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final String currentUserId = currentState.getCurrentUser().getId();
        terminalService.write("ENTER PROJECT NAME: ");
        @NotNull final String name = terminalService.enterIgnoreEmpty();
        if (!projectService.contains(name, currentUserId)) {
            terminalService.write("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
            terminalService.separateLines();
            return;
        }
        @Nullable final Project project = projectService.findByName(name, currentUserId);
        if (project == null) throw new Exception();
        terminalService.write("Use \"-save\" to finish entering, and save information.");
        terminalService.write("ENTER PROJECT INFORMATION: ");
        @NotNull final String newInfo = terminalService.read();
        project.setInfo(newInfo);
        terminalService.write("[UPDATED]");
        terminalService.write("Project successfully updated!");
        terminalService.separateLines();
    }

}
