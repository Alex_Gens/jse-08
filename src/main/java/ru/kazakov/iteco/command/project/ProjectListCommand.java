package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Project;
import java.util.List;

@NoArgsConstructor
public final class ProjectListCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-list";

    @Getter
    @NotNull
    private final String description = "Show all projects.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String currentUserId = currentState.getCurrentUser().getId();
        @Nullable final List<Project> projects = projectService.findAll(currentUserId);
        if (projects == null || projects.isEmpty()) {
            terminalService.write("Project list is empty. Use \"project-create\" to create project.");
            terminalService.separateLines();
            return;
        }
        terminalService.write("[PROJECTS LIST]");
        int counter = 1;
        for (Project project : projects) {
            System.out.println(counter + ". " + project.getName());
            counter++;
        }
        terminalService.separateLines();
    }

}
