package ru.kazakov.iteco.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.command.AbstractCommand;

@NoArgsConstructor
public abstract class ProjectAbstractCommand extends AbstractCommand {

    @Nullable
    protected IProjectService projectService;

    @Override
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
        this.projectService = serviceLocator.getProjectService();
    }

}
