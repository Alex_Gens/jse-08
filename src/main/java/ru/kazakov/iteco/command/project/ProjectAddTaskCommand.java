package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;

@NoArgsConstructor
public final class ProjectAddTaskCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-add-task";

    @Getter
    @NotNull
    private final String description = "Add task to project.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        terminalService.write("ENTER PROJECT NAME: ");
        @Nullable final String currentUserId = currentState.getCurrentUser().getId();
        @NotNull final String projectName = terminalService.enterIgnoreEmpty();
        if (!projectService.contains(projectName, currentUserId)) {
            terminalService.write("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
            terminalService.separateLines();
            return;
        }
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        terminalService.write("ENTER TASK NAME: ");
        @NotNull final String taskName = terminalService.enterIgnoreEmpty();
        if (!taskService.contains(taskName, currentUserId)) {
            terminalService.write("Task with this name doesn't exist. Use \"task-list\" to show all tasks.");
            terminalService.separateLines();
            return;
        }
        @Nullable final Project project = projectService.findByName(projectName, currentUserId);
        @Nullable final Task task = taskService.findByName(taskName, currentUserId);
        if (project == null) throw new Exception();
        if (task == null) throw new Exception();
        if (task.getProjectId() == null) {
            task.setProjectId(project.getId());
            terminalService.write("Task successfully added!");
            terminalService.separateLines();
            return;
        }
        if (task.getProjectId().equals(project.getId())) {
            terminalService.write("Task is already added. Use project-list-tasks to see tasks in project.");
            terminalService.separateLines();
            return;
        }
        task.setProjectId(project.getId());
        terminalService.write("Task successfully added!");
        terminalService.separateLines();
    }

}
