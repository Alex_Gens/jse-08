package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class ProjectListTasksCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-list-tasks";

    @Getter
    @NotNull
    private final String description = "Show all tasks in project.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final String currentUserId = currentState.getCurrentUser().getId();
        terminalService.write("ENTER PROJECT NAME: ");
        @NotNull final String projectName = terminalService.enterIgnoreEmpty();
        if (!projectService.contains(projectName, currentUserId)) {
            terminalService.write("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
            terminalService.separateLines();
            return;
        }
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final Project project = projectService.findByName(projectName, currentUserId);
        if (project == null) throw new Exception();
        @Nullable final List<Task> tasks = taskService.findAll(currentUserId);
        if (tasks == null) throw new Exception();
        @NotNull final List<String> taskIds = new ArrayList<>();
        for (Task task : tasks) {
            if (task.getProjectId().equals(project.getId())) taskIds.add(task.getId());
        }
        if (taskIds.isEmpty()) {
            terminalService.write("The project has no tasks. Use project-add-task to add task to project.");
            terminalService.separateLines();
            return;
        }
        int counter = 1;
        terminalService.write("[TASKS LIST]");
        for (String taskId : taskIds) {
            System.out.println(counter + ": " + taskService.getName(taskId));
            counter++;
        }
        terminalService.separateLines();
    }

}
