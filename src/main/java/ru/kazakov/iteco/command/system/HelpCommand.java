package ru.kazakov.iteco.command.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import java.util.List;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {

    private final boolean secure = false;

    @Getter
    @NotNull
    private final String name = "help";

    @Getter
    @NotNull
    private final String description = "Show all commands.";

    @Override
    public boolean isSecure() {return secure;}

    @Override
    public void execute() throws Exception {
        if (terminalService == null) throw new Exception();
        @Nullable final User currentUser = currentState.getCurrentUser();
        @NotNull final List<AbstractCommand> commands = currentState.getCommands();
        if (currentUser == null || currentUser.getRoleType() != RoleType.ADMINISTRATOR) {
            commands.forEach(v -> {
                if (!v.isAdmin()){
                    System.out.println(v.getName() + ": " + v.getDescription());
                }});
            terminalService.separateLines();
            return;
        }
        commands.forEach(v -> {System.out.println(v.getName() + ": " + v.getDescription());});
        terminalService.separateLines();
    }

}
