package ru.kazakov.iteco.command.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.command.AbstractCommand;

@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {

    public final boolean secure = false;

    @Getter
    @NotNull
    private final String name = "exit";

    @Getter
    @NotNull
    private final String description = "Close task manager.";

    @Override
    public boolean isSecure() {return secure;}

    @Override
    public void execute() throws Exception {
        if (terminalService == null) throw new Exception();
        terminalService.write("*** MANAGER CLOSED ***");
        System.exit(0);
    }

}
