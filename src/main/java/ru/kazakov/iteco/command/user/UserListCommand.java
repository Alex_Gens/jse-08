package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import java.util.List;

@NoArgsConstructor
public final class UserListCommand extends  UserAbstractCommand {

    @Getter
    @NotNull
    private final String name = "user-list";

    @Getter
    @NotNull
    private final String description = "Show all users.   [" + RoleType.ADMINISTRATOR.getDisplayName().toUpperCase() + "]";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (userService == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final List<User> users = userService.findAll();
        if (users == null || users.isEmpty()) {
            terminalService.write("User list is empty.");
            terminalService.separateLines();
            return;
        }
        terminalService.write("[USERS LIST]");
        int counter = 1;
        for (User user : users) {
            System.out.println(counter + ". " + user.getLogin());
            counter++;
        }
        terminalService.separateLines();
    }

}
