package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.entity.Task;

@NoArgsConstructor
public final class TaskCreateCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-create";

    @Getter
    @NotNull
    private final String description = "Create new task.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (taskService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final String currentUserId = currentState.getCurrentUser().getId();
        terminalService.write("ENTER TASK NAME: ");
        @NotNull final String name = terminalService.enterIgnoreEmpty();
        if (taskService.contains(name, currentUserId)) {
            terminalService.write("[NOT CREATED]");
            terminalService.write("Project with this name is already exists. Use another project name.");
            terminalService.separateLines();
            return;
        }
        @NotNull final Task task = new Task(currentUserId);
        task.setName(name);
        taskService.persist(task);
        terminalService.write("[CREATED]");
        terminalService.write("Task successfully created!");
        terminalService.separateLines();
    }

}
