package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Task;
import java.util.List;

@NoArgsConstructor
public final class TaskListCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-list";

    @Getter
    @NotNull
    private final String description = "Show all tasks.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (taskService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final String currentUserId = currentState.getCurrentUser().getId();
        @Nullable final List<Task> tasks = taskService.findAll(currentUserId);
        if (tasks == null || tasks.isEmpty()) {
            terminalService.write("Task list is empty. Use \"task-create\" to create task.");
            terminalService.separateLines();
            return;
        }
        terminalService.write("[TASKS LIST]");
        int counter = 1;
        for (Task task : tasks) {
            System.out.println(counter + ". " + task.getName());
            counter++;
        }
        terminalService.separateLines();
    }

}
