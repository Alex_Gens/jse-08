package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Task;

@NoArgsConstructor
public final class TaskGetCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-get";

    @Getter
    @NotNull
    private final String description = "Show all task information.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (taskService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final String currentUserId = currentState.getCurrentUser().getId();
        terminalService.write("ENTER TASK NAME: ");
        @NotNull final String name = terminalService.enterIgnoreEmpty();
        if (!taskService.contains(name, currentUserId)) {
            terminalService.write("Task with this name doesn't exist. Use \"task-list\" to show all tasks.");
            terminalService.separateLines();
            return;
        }
        @Nullable final Task task = taskService.findByName(name, currentUserId);
        if (task == null) throw new Exception();
        if (taskService.isEmpty(task.getId())) {
            terminalService.write("Task is empty. Use \"task-update\" to update this task.");
            terminalService.separateLines();
            return;
        }
        terminalService.write("[Task: " + name + "]");
        System.out.println(task.getInfo());
        terminalService.separateLines();
    }

}
