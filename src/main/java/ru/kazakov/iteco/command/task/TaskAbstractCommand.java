package ru.kazakov.iteco.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.command.AbstractCommand;

@NoArgsConstructor
public abstract class TaskAbstractCommand  extends AbstractCommand {

    @Nullable
    protected ITaskService taskService;

    @Override
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
        this.taskService = serviceLocator.getTaskService();
    }

}
