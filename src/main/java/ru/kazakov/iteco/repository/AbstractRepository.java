package ru.kazakov.iteco.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.IRepository;
import ru.kazakov.iteco.entity.AbstractEntity;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    protected final Map<String, T> entities = new LinkedHashMap<>();

    @Override
    public void merge(@NotNull final T entity) {entities.merge(entity.getId(), entity, (oldVal, newVal) -> newVal);}

    @Override
    public void persist(@NotNull final T entity) {entities.putIfAbsent(entity.getId(), entity);}

    @Override
    public void remove(@NotNull final String id) {entities.remove(id);}

    @Override
    public void removeAll() {entities.clear();}

    @Nullable
    @Override
    public T findOne(@NotNull final String id) {return entities.get(id);}

    @NotNull
    @Override
    public List<T> findAll() {return new ArrayList<>(entities.values());}

    @Override
    public boolean isEmpty() {return entities.isEmpty();}

}
