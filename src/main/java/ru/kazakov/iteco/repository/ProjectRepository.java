package ru.kazakov.iteco.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.entity.Project;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public String getName(@NotNull final String id) {return entities.get(id).getName();}

    @Override
    public void merge(@NotNull final Project entity) {
        entities.merge(entity.getId(), entity, (v1, v2)
                -> {v1.setInfo(v2.getInfo());
            return v1;});
    }

    @Override
    public void remove(@NotNull final List<String> ids) {
        entities.entrySet().removeIf(entry
                -> ids.contains(entry.getValue().getId()));
    }

    @Override
    public void removeAll(@NotNull final String currentUserId) {
        entities.entrySet().removeIf(entry -> entry.getValue().getUserId().equals(currentUserId));
    }

    @Nullable
    @Override
    public Project findByName(@NotNull final String name) {
        return entities.values().stream()
                .filter(v -> v.getName().equals(name))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Project findByName(@NotNull final String name, @NotNull final String currentUserId) {
        return entities.values().stream()
                .filter(v -> v.getName().equals(name) && v.getUserId().equals(currentUserId))
                .findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final List<String> ids) {
        return entities.values().stream()
                .filter( v -> ids.contains(v.getId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String currentUserId) {
        return  entities.values().stream()
                .filter( v -> v.getUserId().equals(currentUserId))
                .collect(Collectors.toList());
    }

    @Override
    public boolean contains(@NotNull final String name) {
        return entities.values().stream()
                .anyMatch(v -> v.getName().equals(name));
    }

    @Override
    public boolean contains(@NotNull final String name, @NotNull final String currentUserId) {
        return entities.values().stream()
                .anyMatch(v -> v.getName().equals(name) && v.getUserId().equals(currentUserId));
    }

    @Override
    public boolean isEmpty(@NotNull final String id) {return entities.get(id).isEmpty();}

}
