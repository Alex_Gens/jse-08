package ru.kazakov.iteco.context;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.api.service.ITerminalService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.repository.ProjectRepository;
import ru.kazakov.iteco.repository.TaskRepository;
import ru.kazakov.iteco.repository.UserRepository;
import ru.kazakov.iteco.service.ProjectService;
import ru.kazakov.iteco.service.TaskService;
import ru.kazakov.iteco.service.TerminalService;
import ru.kazakov.iteco.service.UserService;
import ru.kazakov.iteco.util.Password;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator, CurrentState {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @NotNull
    private final ServiceLocator serviceLocator = this;

    @NotNull
    private final CurrentState currentState = this;

    @Getter
    @Setter
    @Nullable
    private User currentUser = null;

    public void init(@NotNull final Class<?>[] classes) throws Exception {
        registry(classes);
        addUsers();
        start();
    }

    private void start() throws Exception {
        terminalService.write("*** WELCOME TO TASK MANAGER ***");
        @NotNull String command = "";
        while (true) {
            if (currentUser == null) {
                terminalService.write("You are not authorized. Use \"user-login\" for authorization.");
                terminalService.separateLines();
            }
            command = terminalService.enterIgnoreEmpty();
            command = command.trim().toLowerCase();
            execute(command);
        }
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommands() {return new ArrayList<>(commands.values());}

    private void registry(@NotNull final Class<?>[] classes) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        for (Class<?> clazz : classes
             ) {
            if (!AbstractCommand.class.isAssignableFrom(clazz)) continue;
            @NotNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();
            command.setServiceLocator(serviceLocator);
            command.setCurrentState(currentState);
            @NotNull final String name = command.getName();
            commands.put(name, command);
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        if (!commands.containsKey(command)) {
            terminalService.write("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
            return;
        }
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        if (currentUser == null && abstractCommand.isAdmin()) {
            terminalService.write("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
            return;
        }
        if (currentUser == null && abstractCommand.isSecure()) {
            terminalService.write("[NO ACCESS]");
            return;
        }
        if (currentUser == null && !abstractCommand.isSecure()) {
            abstractCommand.execute();
            return;
        }
        if (currentUser.getRoleType() != RoleType.ADMINISTRATOR && abstractCommand.isAdmin()) {
            terminalService.write("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
            terminalService.separateLines();
            return;
        }
        abstractCommand.execute();
    }

    private void addUsers() throws Exception {
        @NotNull final User user = new User();
        user.setLogin("user");
        @NotNull final String password = Password.getHashedPassword("pass");
        user.setPassword(password);
        user.setRoleType(RoleType.DEFAULT);
        @NotNull final User admin = new User();
        admin.setLogin("admin");
        admin.setPassword(password);
        admin.setRoleType(RoleType.ADMINISTRATOR);
        userService.persist(user);
        userService.persist(admin);
    }

}
